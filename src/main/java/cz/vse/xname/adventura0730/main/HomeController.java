package cz.vse.xname.adventura0730.main;

import cz.vse.xname.adventura0730.logika.Hra;
import cz.vse.xname.adventura0730.logika.IHra;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

public class HomeController {

    private IHra hra = new Hra();

    @FXML
    private TextArea vystup;
    @FXML
    private TextField vstup;

    @FXML
    private void initialize() {
        vystup.appendText(hra.vratUvitani()+"\n\n");
        Platform.runLater(() -> vstup.requestFocus());
    }

    @FXML
    private void zpracujVstup() {
        String prikaz = vstup.getText();
        vstup.clear();
        vystup.appendText("> "+prikaz+"\n");
        String vysledek = hra.zpracujPrikaz(prikaz);
        vystup.appendText(vysledek+"\n\n");

        if(hra.isKonecHry()) {
            vystup.appendText(hra.vratEpilog());
            vstup.setDisable(true);
        }
    }
}
