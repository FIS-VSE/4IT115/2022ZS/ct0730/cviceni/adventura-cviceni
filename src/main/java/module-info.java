module cz.vse.xname.adventura0730 {
    requires javafx.controls;
    requires javafx.fxml;


    opens cz.vse.xname.adventura0730.main to javafx.fxml;
    exports cz.vse.xname.adventura0730.main;
}